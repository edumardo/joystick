# Dual-axis XY Joystick library for Arduino
Just a simple arduino library for control dual axis joystick with no action limit.

## Install the library
Download this repository as a .zip file and from the Arduino IDE go to *Sketch -> Include library -> Add .ZIP Library*

## Import
You can import the library in your code using the Arduino IDE going to *Sketch -> Include library -> Joystick*
or directly writing the include statement in your code:
```
#include "Joystick.h"
```
